# -*- coding: utf-8 -*-

"""Main module."""
import abc
import copy
import importlib
import inspect
import logging
import sys
from collections import Sequence
from typing import Type, Dict, Union, Any, List, Mapping, Tuple

from stevedore import ExtensionManager

from frtls.classes import get_all_subclasses, get_class_from_string
from frtls.lists import ensure_sequence
from frtls.strings import from_camel_case

log = logging.getLogger("plugtings")


def _create_plugting_index_extension_manager() -> ExtensionManager:
    """Loading all plugting type details.

    The result is a dict containing the Plugting alias as key and its type as value.
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stderr)
    out_hdlr.setFormatter(logging.Formatter("plugting load error -> %(message)s"))
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading plugting subclasses...")

    mgr = ExtensionManager(
        namespace="plugting_index_types",
        invoke_on_load=False,
        propagate_map_exceptions=True,
    )

    return mgr


def load_registered_plugting_types() -> Dict[str, Type]:
    """Load all Plugting types that are registered under the entrypoint 'plugtings'."""

    mgr = _create_plugting_index_extension_manager()

    result = {}
    for t in mgr:
        result[t.name] = t.plugin
    return result


def load_entry_point_plugting(entry_point: str) -> "Plugting":
    """Create a Plugting object from an entry point."""

    index = EntryPointIndex(entry_point=entry_point)
    if not index.plugins:
        raise Exception(
            f"Can't create index for entry_point '{entry_point}': no plugins found"
        )
    return Plugting(index=index)


def load_subclass_plugting(
    base_class: Union[Type, str], ensure_loaded: Union[str, List[str]] = None
) -> "Plugting":
    """Create a Plugting object from a base class or base class string."""

    index = SubclassPluginIndex(base_class=base_class, ensure_loaded=ensure_loaded)
    if not index.plugins:
        raise Exception(
            f"Can't create index for sub-class '{base_class}': no subclasses found"
        )
    p = Plugting(index=index)
    return p


def load_plugting(entry_point_or_base_class: str) -> "Plugting":
    """Create a Plugin object, try to auto-determine the type of the input (base class or entry point).

    Base class input will be tried first.
    """

    try:
        p = load_subclass_plugting(entry_point_or_base_class)
    except (Exception):
        try:
            p = load_entry_point_plugting(entry_point_or_base_class)
        except (Exception):
            raise Exception(f"Can't create plugting for '{entry_point_or_base_class}'.")

    return p


def ensure_object_list(entry_point_or_base_class: Union[Type, str], data: List[Any]):

    if not isinstance(data, Sequence):
        raise ValueError("'data' needs to be a sequence")

    result = []
    for item in data:
        r = ensure_object(entry_point_or_base_class, item)
        result.append(r)

    return result


def ensure_object(entry_point_or_base_class: Union[Type, str], data: Any):
    """Auto-magic method to ensure the provided data is of the specified type.

    TODO: documentation
    """

    if isinstance(entry_point_or_base_class, Type):
        if isinstance(data, entry_point_or_base_class) or issubclass(
            data.__class__, entry_point_or_base_class
        ):
            return data
        elif isinstance(data, Mapping):

            plugting = load_subclass_plugting(entry_point_or_base_class)
            obj = plugting.create_obj(init_data=data)
            return obj

        else:
            raise Exception(
                f"Can't ensure provided data to be of type '{entry_point_or_base_class.__name__}': invalid data type '{data.__class__.__name__}'",
                data,
                entry_point_or_base_class,
            )
    elif isinstance(entry_point_or_base_class, str):
        plugting = load_entry_point_plugting(entry_point_or_base_class)

        obj = plugting.create_obj(init_data=data)
        return obj
    else:
        raise ValueError(
            f"Invalid type '{type(entry_point_or_base_class)}' for 'entry_point_or_base_class' argument, needs to be a Type or string."
        )


class PlugtingPluginIndex(metaclass=abc.ABCMeta):
    """A class that takes care of the loading of plugins within a `Plugting` object.
    """

    @property
    def alias(self) -> str:
        return self._get_alias()

    @abc.abstractmethod
    def _get_alias(self) -> str:
        pass

    @property
    def plugins(self) -> Dict[str, Type]:
        return self._get_plugins()

    @abc.abstractmethod
    def _get_plugins(self) -> Dict[str, Type]:
        pass


class SubclassPluginIndex(PlugtingPluginIndex):
    """An index that finds (loaded) subclasses of a base class and auto-generates aliases based on the class name and/or path."""

    def __init__(
        self, base_class: Union[str, Type], ensure_loaded: Union[str, List[str]] = None
    ):

        if isinstance(base_class, str):
            base_class = get_class_from_string(base_class)
        elif base_class is not None and not isinstance(base_class, Type):
            raise ValueError(
                f"Invalid type for argument 'base_class': needs to be 'Type' or 'str', not {type(base_class)}"
            )

        self._base_class = base_class
        self._ensure_loaded_modules = ensure_sequence(ensure_loaded, convert_none=True)

        self._plugins = self._load_subclasses()
        self._alias = base_class.__name__

    def _get_alias(self) -> str:
        return self._alias

    def _get_plugins(self) -> Dict[str, Type]:
        return self._plugins

    def _load_subclasses(self) -> Dict[str, Type]:
        """Load all subclasses of the base class and use the class name as alias.

        The lowercase class name will be also added as an additional alias. If the class name ends with 'Property',
        another alias without that postfix will be added, converting CamelCase to tokens seperated by '_'.
        """

        for mod in self._ensure_loaded_modules:
            importlib.import_module(mod)

        temp = get_all_subclasses(self._base_class)

        plugins = {}

        if not inspect.isabstract(self._base_class):
            temp.append(self._base_class)

        for pl in temp:
            name = pl.__name__

            name_lower = from_camel_case(name, sep="-")

            if name_lower in plugins.keys():
                log.warning(f"Duplicate class name '{name}': ignoring class: {pl}")
                continue

            plugins[name_lower] = pl
            plugins[name] = pl

        return plugins


class EntryPointIndex(PlugtingPluginIndex):
    """Class to load plugins via their Python entry point."""

    def __init__(self, entry_point: str):

        self._entry_point = entry_point

        if not entry_point:
            raise ValueError(
                "Can't create EntryPointIndex: entry_point argument can't be empty."
            )

        self._plugins = self._load_from_entrypoints()

        if not self._plugins:
            raise ValueError(f"No plugins available for entry_point '{entry_point}'")
        self._alias = entry_point

    def _get_alias(self) -> str:
        return self._alias

    def _get_plugins(self) -> Dict[str, Type]:
        return self._plugins

    def _load_from_entrypoints(self) -> Dict[str, Type]:
        """Loading all subclasses of a base class."

        The result is a dict containing the alias of the subclass (the entrypoint) as key and its type as value.
        """

        log2 = logging.getLogger("stevedore")
        out_hdlr = logging.StreamHandler(sys.stderr)
        out_hdlr.setFormatter(
            logging.Formatter(f"{self._entry_point} load error -> %(message)s")
        )
        out_hdlr.setLevel(logging.DEBUG)
        log2.addHandler(out_hdlr)
        log2.setLevel(logging.INFO)

        log.debug(f"Loading subclasses for entry point {self._entry_point}...")

        mgr = ExtensionManager(
            namespace=self._entry_point,
            invoke_on_load=False,
            propagate_map_exceptions=True,
        )

        plugins = {}

        for t in mgr:

            plugins[t.name] = t.plugin

        return plugins


class Plugting(object):
    """Class to manage and (auto-)load subclasses of (most of the time: abstract) classes.

    Args:

        - *index*: the index ot use to find plugins
    """

    def __init__(self, index: PlugtingPluginIndex):

        self._index = index

    @property
    def alias(self) -> str:
        """Return the alias for this Plugting."""
        return self._index.alias

    @property
    def plugins(self) -> Dict[str, Type]:

        return self._index._plugins

    def get_default_init_data(self, alias: str) -> Dict:
        """Return the default init data for value of the specified alias/entrypoint.

        Returns an empty dict by default, but can be overwritten by sub-classes.
        """

        return {}

    def get_default_plugin(self) -> str:
        """Return the default plugin for this Pluging.

        Returns `None` by default, but can be overwritten by sub-classes.
        """

        return None

    def get_base_class(self) -> Type:
        """Return the default base class every instantiated object of this `Plugting` needs to equal or inherit from.

        Returns `None` by default, but can be overwritten by a sub-class. If `None` is returned, no
        type-checking will be done
        """

        return None

    def create_plugin_class(
        self, entry_point: str = None, init_data: Union[str, Dict] = None
    ) -> Tuple[Type, Dict]:
        """Create the class for the input data.

        If the init data is empty or does not contain the '_type' key, the default type of this Plugting is used.
        If this Plugting does not have a default type, a ValueError is thrown.

        If the entry_point value is empty, this `Plugting` will check whether the `init_data` is a or string mapping.
        If a string, this will be used as entry_point. If mapping and it has a "_type" key, that value will be used
        as entry_point. If at this point there is still no entry_point available, the default entry_point of this
        Plugting will be used. If that also doesn't exist, an Exception will be thrown.

        Args:

          - *entry_point*: the alias of the plugin for which to create an object
          - *init_data*: an already initialized object, or a dictionary containing the init data to be used to construct a new object.

        Returns:

          - a tuple including the plugin class and init data (cleaned from any potential type/class information)

        """

        # if self.get_base_class():
        #     if (
        #         issubclass(init_data.__class__, self.get_base_class())
        #         or init_data.__class__ == self.get_base_class()
        #     ):
        #         return init_data

        if not init_data:
            if entry_point is not None:
                p_type = entry_point
            else:
                p_type = self.get_default_plugin()

            if not p_type:
                raise ValueError(
                    f"Can't generate object of namespace '{self.alias}': no default type available for this Plugting. Please specify the '_type' key in your init data."
                )
            init_data = self.get_default_init_data(alias=p_type)
            if not init_data:
                init_data = {}
            if not isinstance(init_data, Mapping):
                raise TypeError(
                    f"Can't initialize object for '{p_type}': invalid data type '{type(init_data)}' (needs a Mapping)."
                )
        else:

            if isinstance(init_data, str):
                if entry_point is None:
                    init_data = {"_type": init_data}
                else:
                    raise ValueError(
                        f"Invalid type for 'init_data', can only be string if no entry_point is provided"
                    )

            if not isinstance(init_data, Mapping):
                raise TypeError(
                    f"Can't initialize Plugting plugin object: invalid data type '{type(init_data)}' (needs a Mapping, or string)."
                )

            init_data = copy.deepcopy(init_data)
            if entry_point is not None:
                p_type = entry_point
            else:
                p_type = init_data.pop("_type", None)

            if p_type is None:
                p_type = self.get_default_plugin()
                if not p_type:

                    unique = None
                    for v in self.plugins.values():
                        if unique is None:
                            unique = v
                            continue

                        if v != unique:
                            unique = None
                            break

                    if unique is None:
                        raise ValueError(
                            f"Can't generate object of namespace '{self.alias}': no default type available for this Plugting. Please specify the '_type' key in your init data."
                        )

                    p_type = next(iter(self.plugins.keys()))
                    log.debug(
                        f"No type alias specified, but only one type possible, so taking that: {unique}"
                    )

        cls = self.plugins.get(p_type, None)
        if cls is None:
            raise ValueError(
                f"Can't create object of namespace '{self.alias}': no type '{p_type}' available."
            )

        if self.get_base_class() is not None:

            if not cls == self.get_base_class() and not issubclass(
                cls, self.get_base_class()
            ):
                raise Exception(
                    f"Invalid base class: {self.get_base_class().__name__} != {cls.__name__}"
                )

        return (cls, init_data)

    def create_obj(
        self, entry_point: str = None, init_data: Union[str, Dict] = None
    ) -> Any:
        """Create an object for the input data.

        Returns the input data if it is an already initialized object of a valid type for this Plugting. If the init
        data is empty or does not contain the '_type' key, the default type of this Plugting is used. If this Plugting
        does not have a default type, a ValueError is thrown.

        If the entry_point value is empty, this `Plugting` will check whether the `init_data` is a or string mapping.
        If a string, this will be used as entry_point. If mapping and it has a "_type" key, that value will be used
        as entry_point. If at this point there is still no entry_point available, the default entry_point of this
        Plugting will be used. If that also doesn't exist, an Exception will be thrown.

        - *entry_point*: the alias of the plugin for which to create an object
        - *init_data*: an already initialized object, or a dictionary containing the init data to be used to construct a new object.
        """

        if self.get_base_class():
            if (
                issubclass(init_data.__class__, self.get_base_class())
                or init_data.__class__ == self.get_base_class()
            ):
                return init_data

        cls, data = self.create_plugin_class(
            entry_point=entry_point, init_data=init_data
        )

        try:
            obj = cls(**data)
        except (Exception) as e:
            raise Exception(f"Can't create object of class '{cls}': {e}")
        return obj
